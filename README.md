# ej-disco

Implementation of the [ABCDisco method](https://arxiv.org/abs/2007.14400) for the Run 3 Emerging Jets analysis.

## Setup

Start in a fresh virtual environment:

```
python3 -m venv env
source env/bin/activate
```

Once inside virtual environment and in the top level directory of the repo, you
can install the `disco` package and it's dependencies via `pip` using
```
python -m pip install -e .
pip install -r requirements.txt
```

