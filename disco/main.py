"""Top level training script, powered by the lightning CLI."""

import pathlib

import comet_ml  # noqa F401
from lightning.pytorch.cli import ArgsType


def main(args: ArgsType = None) -> None:
    config_dir = pathlib.Path(__file__).parent / "configs"

if __name__ == "__main__":
    main()
